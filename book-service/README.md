## Book Service

*****WORK IN PROGRESS ****

Demo Microservice and integration/component tests with testcontainers 

System in high level:

```
client --> microservice --> database
``` 

Highlights of technologies:
```
* spring-boot
* H2 (database for unit test)
* postgres (database for production and integration/component test)
* JPA
* Flyway (database migration)
* Docker
* testcontainers (for integration/conponent test)
* Junit5
* RestAssured
* Hamcrest
```

To build the service

`mvn clean package`
 
To run the service in standalone mode (with embedded H2 database by default)
```
cd book-service
mvn spring-boot:run
```

To build the service's docker container with default tag (local)

`sh docker-build.sh`

To build the service's docker container with your-tag

`sh docker-build.sh your-tag`

To run service and postgresql with docker-compose  (make sure the docker container has been built)

`docker-compose up`

To run integration/component tests with service and postgresql (make sure the docker container has been built) 

`mvn integration-test`

Highlights of CICD build steps: 
```
- mvn clean package
- build docker image with tag local (sh docker-build.sh or equvelent to build docker image)
- mvn integration-test
- tag container image with release tag, and push to release docker-repo
* note: in any of above steps, stop and exit if any step fails  
```


