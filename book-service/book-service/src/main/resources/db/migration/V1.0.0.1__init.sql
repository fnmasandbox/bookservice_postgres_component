CREATE SCHEMA if NOT EXISTS book;

CREATE TABLE book.book
(
    id uuid PRIMARY KEY,
    version INTEGER NOT NULL,
    isbn VARCHAR(16) NOT NULL UNIQUE,
    title VARCHAR(128) NOT NULL,
    author VARCHAR(128) NOT NULL,
    summary VARCHAR(1600)
)
;
CREATE INDEX book_isbn_idx on book.book(isbn);
CREATE INDEX book_author_idx on book.book(author);

