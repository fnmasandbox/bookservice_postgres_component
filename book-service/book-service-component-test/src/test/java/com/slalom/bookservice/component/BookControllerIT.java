package com.slalom.bookservice.component;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import static org.hamcrest.MatcherAssert.assertThat;

@Slf4j
@Testcontainers //for Jupiter/Junit5 integration
public class BookControllerIT {
    private static final String serviceName = "book-service";
    private static final Integer servicePort = 9001;

    private String bookServiceUri;

    static Network network = Network.newNetwork();

    // -- set up the testcontainers for containers in the test
    // TODO: abstract followings to base class so it can be shared by more test classes
    // TODO: abstract the config values into a config file. Leave them here for demo purpose
    @Container // will be started before the first method, and stopped after last method
    public static GenericContainer postgreDb = new GenericContainer(DockerImageName.parse("postgres:11.1"))
            .withNetwork(network)
            .withNetworkAliases("postgres-db") // use this so it can be referenced by others
            .withExposedPorts(5432)
            .withEnv("POSTGRES_USER", "postgres")
            .withEnv("POSTGRES_PASSWORD", "postgres")
            .withEnv("POSTGRES_DB", "book");

    // TODO: enhance workflow to build the image fresh. Remove the manual steps of building and keeping an image up to date
    @Container // will be started before the first method, and stopped after last method
    public static GenericContainer bookService = new GenericContainer(DockerImageName.parse("slalom/book-service:local"))
            .withNetwork(network)
            .withExposedPorts(9001)
            .withEnv("POSTGRES_USER", "postgres")
            .withEnv("POSTGRES_PASSWORD", "postgres")
            .withEnv("POSTGRESQL_HOST", "postgres-db") // reference to a dependency
            .withEnv("POSTGRESQL_PORT", "5432");

    @BeforeEach
    void setUp() {
        String bookServiceHost = bookService.getHost();
        Integer bookServicePort = bookService.getFirstMappedPort();

        bookServiceUri = getBookServiceUri(bookServiceHost, bookServicePort.toString());
        log.info("bookServiceUri=" + bookServiceUri);
    }

    @Test
    public void testAddBook() {
        String isbn = generateIsbn();
        // when: add a new book
        Response response = restPost(createBook(isbn), HttpStatus.SC_CREATED);

        // then: the newly added book should be added and returned
        JsonPath jsonPath = response.getBody().jsonPath();
        assertThat(jsonPath.get("id"), Matchers.notNullValue());
        assertThat(jsonPath.get("isbn"), Matchers.is(isbn));
    }

    @Test
    public void testGetBooks() {
        // Given: two newly added books
        String isbn1 = generateIsbn();
        restPost(createBook(isbn1), HttpStatus.SC_CREATED);
        String isbn2 = generateIsbn();
        restPost(createBook(isbn2), HttpStatus.SC_CREATED);

        // when: call get books
        Response response = restGet(HttpStatus.SC_OK);

        // then: the two newly added books should be among the returned books
        JsonPath jsonPath = response.getBody().jsonPath();
        log.info(jsonPath.toString());
        assertThat(jsonPath.getList("isbn"), Matchers.containsInAnyOrder(isbn1, isbn2));
    }

    // TODO: add tests for error conditions

    // -----------------------------------
    // -- utilities

    private String getBookServiceUri(String host, String port) {
        return String.format("http://%s:%s", host, port);
    }

    private String generateIsbn() {
        // randomize the ISBN to ensure the uniqueness
        String isbn = String.format("%s-%s", RandomStringUtils.random(3, false, true), RandomStringUtils.random(10, false, true));
        return isbn;
    }

    private String createBook(String isbn) {
        // TODO: export/import rest-api and objects (open-api) from book-service
        String bookJson = String.format("{\"isbn\": \"%s\",\"author\": \"Jeff Kinney\",\"title\": \"The Deep End (Diary of a Wimpy Kid Book 15)\"}", isbn);
        log.info("{}", bookJson);
        return bookJson;
    }

    // -----------------------------------
    // -- REST utilities with RestAssured
    private Response restGet(int expectedStatus) {
        Response response = RestAssured.given().baseUri(bookServiceUri)
                .get("/v1/books")
                .then()
                .statusCode(expectedStatus)
                .extract()
                .response();
        return response;
    }

    private Response restPost(String bodyText, int expectedStatus) {
        Response response = RestAssured.given().baseUri(bookServiceUri)
                .contentType("application/json")
                .body(bodyText)
                .post("/v1/books")
                .then()
                .statusCode(expectedStatus)
                .extract().response();
        return response;
    }
}