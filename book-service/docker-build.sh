#! /usr/bin/env bash

set -e
SERVICE=book-service

ORG=slalom
TAG="${1:-local}"

pushd ${SERVICE}
  docker build -t ${ORG}/${SERVICE}:${TAG} .
popd