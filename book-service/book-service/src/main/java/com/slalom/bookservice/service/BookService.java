package com.slalom.bookservice.service;

import com.slalom.bookservice.domain.Book;
import com.slalom.bookservice.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class BookService {
    private final BookRepository bookRepository;

    public Book addBook(Book aBook) {
        return bookRepository.save(aBook);
    }

    public List<Book> findAll() {
        return bookRepository.findAll();
    }

}
