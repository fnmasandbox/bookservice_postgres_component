package com.slalom.bookservice.controller;

import com.slalom.bookservice.domain.Book;
import com.slalom.bookservice.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/v1/books")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @GetMapping()
    List<Book> getBooks() {
        return bookService.findAll();
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    Book addBook(@RequestBody Book book) {
        log.debug(book.toString());
        return bookService.addBook(book);
    }
}
