package com.slalom.bookservice.repository;

import com.slalom.bookservice.domain.Book;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
//@ActiveProfiles("test")
class BookRepositoryTest {
    @Autowired
    BookRepository repository;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    // TODO: Add testcontainer test cases. Add 'native' query methods to repository layer.
    @Test
    public void testRepository() {
        Book book = Book.builder()
                .isbn("978-1419748684")
                .title("The Deep End (Diary of a Wimpy Kid Book 15)")
                .author("Jeff Kinney")
                .build();
        repository.save(book);

        assertNotNull(book.getId());

    }
}